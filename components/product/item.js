import Link from 'next/link'

function ProductItem({ product }) {
    return (
        <>
            <Link href="/products/detail/[slug]" as={`/products/detail/${product.slug}-${product.id}`}>
                <a>
                    <h3>{product.title}</h3>
                </a>
            </Link>
        </>
    )
}

export default ProductItem
