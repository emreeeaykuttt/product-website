// import ProductItem from 'item'
import ProductItem from './item'

function ProductList({ products }) {
    return (
        <div className="product-list">
            {products.map((product, i) => (
                <ProductItem product={product} key={i}></ProductItem>
            ))}
        </div>
    )
}

export default ProductList
