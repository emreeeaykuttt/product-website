import Head from 'next/head'
import SiteConfig from '../../site.config'
import Footer from '../include/footer'
import Header from '../include/header'

function UserLayout({ children }) {
    return (
        <>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="description" content={SiteConfig.description} />
                <meta name="keywords" content={SiteConfig.keywords} />
                <title>{SiteConfig.title}</title>
            </Head>

            <Header></Header>

            <main>{children}</main>

            <Footer></Footer>
        </>
    )
}

export default UserLayout
