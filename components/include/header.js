import Link from 'next/link'

function Header() {
    return (
        <div className="menu">
            <Link href="/">ANASAYFA</Link>
            <Link href="/products">ÜRÜNLER</Link>
        </div>
    )
}

export default Header
