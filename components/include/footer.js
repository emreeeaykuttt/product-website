import React, { useContext } from 'react'
import StoreContext from '../../store'
import { THEME } from '../../constants'

function Footer() {
    const store = useContext(StoreContext)

    return (
        <>
            <div className="footer">
                <p>© 2021 Product Website</p>

                <div>
                    Change Theme{' '}
                    <button
                        type="button"
                        onClick={() => store.changeTheme(store.theme === THEME.LIGHT ? THEME.DARK : THEME.LIGHT)}
                    >
                        {store.theme === THEME.LIGHT ? 'Dark' : 'Light'}
                    </button>
                </div>
            </div>
        </>
    )
}

export default Footer
