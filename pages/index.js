import React, { useState } from 'react'
import io from 'socket.io-client'
import DefaultLayout from '../components/layouts/default'

const socket = io('http://localhost:8000', { transports: ['websocket'] })

export default function Home() {
    const [name, setName] = useState('')
    const [list, setList] = useState([])
    const handlepost = (e) => {
        socket.emit('chat', { post: name })
    }
    socket.on('messageSend', (data) => {
        setList([...list, data])
    })

    return (
        <DefaultLayout>
            <h1>ANASAYFA</h1>

            <input type="text" onChange={(e) => setName(e.target.value)} />
            <button onClick={handlepost}>Gönder</button>
            {list.map((value, key) => (
                <div key={key}>
                    <li>{value.post}</li>
                </div>
            ))}
        </DefaultLayout>
    )
}
