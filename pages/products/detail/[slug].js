import DefaultLayout from '../../../components/layouts/default'
import { getProductByID } from '../../../actions/product'
import Head from 'next/head'

function ProductDetail({ product }) {
    return (
        <DefaultLayout>
            <Head>
                <title>{product.title}</title>
            </Head>
            <div className="product-detail">
                <h2>{product.title}</h2>
                <h4>{product.description}</h4>
                <p>{product.detail}</p>
            </div>
        </DefaultLayout>
    )
}

export const getServerSideProps = async (context) => {
    const { params } = context
    const id = params.slug.split('-').slice(-1)[0]
    const product = await getProductByID(id)

    return {
        props: {
            product: product.data
        }
    }
}

export default ProductDetail
