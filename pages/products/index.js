import DefaultLayout from '../../components/layouts/default'
import ProductList from '../../components/product/list'
import { getProducts } from '../../actions/product'

export const API_URL = process.env.API_URL

function Products({ products }) {
    return (
        <DefaultLayout>
            <h1>ÜRÜNLER</h1>
            <ProductList products={products}></ProductList>
        </DefaultLayout>
    )
}

export async function getServerSideProps() {
    const productsData = await getProducts({ sortby: 'sort' })

    return {
        props: {
            products: productsData.data
        }
    }
}

export default Products
