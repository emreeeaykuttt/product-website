import queryString from 'query-string'

export const API_URL = process.env.API_URL

export const getProducts = async (params) => {
    let query = 'published=1'
    query += (params ? '&' : '') + queryString.stringify(params)

    return fetch(API_URL + 'products?' + query, {
        method: 'GET'
    })
        .then((response) => {
            return response.json()
        })
        .catch((err) => console.log(err))
}

export const getProductByID = async (id) => {
    let query = 'published=1'

    return fetch(API_URL + 'products/' + id + '?' + query, {
        method: 'GET'
    })
        .then((response) => {
            return response.json()
        })
        .catch((err) => console.log(err))
}
