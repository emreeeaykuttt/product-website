module.exports = {
    lang: 'tr',
    title: 'Product Website',
    description: 'Product Website Description',
    keywords: 'Product, Website, Keywords',
    author: {
        email: 'emreeeaykutt@gmail.com',
        name: 'Emre Aykut'
    }
}
