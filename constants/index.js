export const THEME = {
    LIGHT: 'theme-light',
    DARK: 'theme-dark'
}

export const Pages = {
    homepage: {
        path: '/',
        name: 'Anasayfa'
    },
    product: {
        path: '/product',
        name: 'Profil'
    }
}
